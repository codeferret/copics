# README #

### What is this repository for? ###

Find the closest (Euclidean distance) Copic marker to a color

### How do I get set up? ###

* Check out the repo
* Run copic.py in IDLE or shell
* Enter a hex code for your color
* You'll get the closest Copic marker
* Enter x to exit


### Who do I talk to? ###

sonja@codeferret.nl