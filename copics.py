#!/usr/bin/env python
import sys


def load_copics(copicsfile):
    copics = {}
    f = open(copicsfile, "r")
    for line in f.readlines():
        marker = line.split(",")
        copics[marker[1]] = marker[0].upper()
    return copics


def get_components(hex):
    return int(hex[:2], 16), int(hex[2:4], 16), int(hex[4:], 16)


def is_color(hex):
    try:
        r, g, b = get_components(hex)
        return r < 256 and g < 256 and b < 256
    except ValueError:
        return False


def get_diff(hex1, hex2):
    r1, g1, b1 = get_components(hex1)
    r2, g2, b2 = get_components(hex2)

    return (r1 - r2) ** 2 + (g1 - g2) ** 2 + (b1 - b2) ** 2


def find_closest(val):
    if not is_color(val):
        print("Not a color hex")
        return None
    closest = None
    min_diff = 3 * 255 ** 2
    for hexcode in copics.keys():
        diff = get_diff(val, hexcode)
        if diff < min_diff:
            min_diff = diff
            closest = copics[hexcode]
    return closest


print("Enter color hex code to find closest Copic marker or X to exit")
copics = load_copics("copics.txt")

if sys.version_info >= (3, 0):
    input_function = input
else:
    input_function = raw_input

while True:
    val = input_function("Color #")
    if val.lower() == "x":
        exit(0)
    closest = find_closest(val)
    if closest:
        print("Copic marker: " + closest)
